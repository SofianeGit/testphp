<?php

	/*class Personnage // Présence du mot-clé class suivi du nom de la classe.
	{
		// Déclaration des attributs et méthodes ici.
	}*/
	
	class Personnage
	{
		private $_force; // La force du personnage, par défaut à 50
		private $_localisation = "Lyon"; // Sa localisation, par défaut à Lyon
		private $_experience; // Son expérience, par défaut à 1
		private $_degats; // Ses dégats, par défaut à 0
		private $_parler;
		
		//public function __construct($experience = 3, $force = 10){
			//$this->_experience = $experience;
			//$this->_force = $force;
		//}
		
		/*	public function afficherExperience() // Une méthode affichant l'attribut $experience du personnage
			{
				echo $this->_experience;
			}
		*/
			public function gagnerExperience() // Une méthode augmentant l'attribut $experience du personnage
			{
				$this->_experience++;
			}
		
		
			public function frapper(Personnage $persoAFrapper)
			{
				$persoAFrapper->_degats += $this->_force;
			}
	
			public function deplacer() // Une méthode qui déplacera le personnage(modifiera donc sa localisation)
			{
			
			}
		
			public function getParler()
			{
				return $this->_parler;
			}
			
			public function setParler($parler)
			{
				$this->_parler = $parler;
			}
			
			// Ceci est la méthode getForce() : elle se charge de renvoyer le contenu de l'attribut $_force
			public function getForce()
			{
				return $this->_force;
			}
			
			// Ceci est la méthode getExperience : elle se charge de renvoyer le contenu de l'attribut $_experience
			public function getExperience()
			{
				return $this->_experience;
			}
			
			// Ceci est la méthode getDegats() : elle se charge de renvoyer le contenu de l'attribut $_degats
			public function getDegats()
			{
				return $this->_degats;
			}
			
			// Setter chargé de modifier l'attribut $_experience
			public function setExperience($experience = 3)
			{
				if (!is_int($experience)) // S'il ne s'agit pas d'un nombre entier.
				{
					trigger_error('L\'expérience d\'un personnage doit être un nombre entier', E_USER_WARNING);
					return;
				}
    
				if ($experience > 100) // On vérifie bien qu'on ne souhaite pas assigner une valeur supérieure à 100.
				{
					trigger_error('L\'expérience d\'un personnage ne peut dépasser 100', E_USER_WARNING);
					return;
				}
    
				$this->_experience = $experience;
			}
			
			
			public function setForce($force)
			{
				$this->_force = $force;
			}
			
			public function setDegats($degats)
			{
				$this->_degats = $degats;
			}
	}

	$perso1 = new Personnage; // Crée un objet, $perso sera un objet de type Personnage
	$perso2 = new Personnage; // objet 2 de type personnage 
	$perso2->setForce($force = 68);
	
	//$perso1->parler(); // Va chercher l'objet $perso et invoque la méthode parler() sur cet objet

	$perso1->setParler("Je suis le personnage numéro 1 et j'ai comme caractéritsiques : ");
	echo $perso1->getParler()."<br/>";
	
	$perso1->setForce($force = 32);
	echo 'Force : ' .  $perso1->getForce() .  "<br/>";
	//echo "Dégats : ".$perso1->getDegats()."<br/>"."<br/>";

	$perso2->frapper($perso1); // On fait frapper le perso 2

	//echo $perso1->getExperience() .  " ";
	
	$perso1->setExperience(12); // Experience à 12
	$perso1->frapper($perso2); // Personnage 1 frappe personnage 2
	$perso1->gagnerExperience(); // Il gagne de l'experience
	echo 'Experience : '.$perso1->getExperience() .  "<br/> "; // Affiche nouvelle experience ( à 13 maintenant)
	
	//$perso1->setDegats();
	echo "Dégats : ".$perso1->getDegats()."<br/>"."<br/>";
	

	$perso2->setParler("Je suis le personnage numéro 2 et j'ai comme caractéristiques : ");
	echo $perso2->getParler()."<br/>";
	
	echo "Force : ".$perso2->getForce()."<br/>";
	
	$perso2->setExperience(17); // Experience à 7
	$perso2->frapper($perso1); // On fait frapper le perso 2
	$perso2->gagnerExperience(); // Il gagne de l'experience
	echo "Expérience: ".$perso2->getExperience()."<br/>"; // Affiche nouvelle experience ( à 8 maintenant)
	
	//$perso2->setDegats();
	echo "Dégats : ".$perso2->getDegats()."<br/>"."<br/>";
	
?>