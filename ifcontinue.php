<?php

$letters = range('A', 'Z');
foreach($letters as $letter){
    echo "$letter <br/>";
    if(in_array($letter, array('A', 'E', 'I',  'U', 'Y'))){
        continue;
    }

    if($letter == 'K'){
        break;
    }
    echo "$letter <br/>";
}

?>