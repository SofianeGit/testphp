<?php

class Voiture
{
	private $_porte;
	private $_volant;
	private $_coffre;
	
	public function getPorte()
	{
		return $this->_porte;
	}
	
	public function setPorte($porte)
	{
		$this->_porte = $porte;
	}
	
	public function getVolant()
	{
		return $this->_volant;
	}
	
	public function setVolant($volant)
	{
		$this->_volant = $volant;
	}
	
	public function getCoffre()
	{
		return $this->_coffre;
	}
	
	public function setCoffre($coffre = "petit")
	{
		$this->_coffre = $coffre;
	}
}

$nut1 = new Voiture;
$nut2 = new Voiture;

$nut1->setPorte($porte = 89);
echo "La porte a ".$nut1->getPorte()." rayures et ";

$nut1->setVolant($volant = "moche");
echo "le volant est ".$nut1->getVolant();

$nut1->setCoffre("spacieux");
echo " mais le coffre est ".$nut1->getCoffre()."<br/>"."<br/>";

$nut2->setPorte(2);
echo "La porte de la 2nde voiture a ".$nut2->getPorte()." rayures";

$nut2->setVolant("sympa");
echo " et le volant est ".$nut2->getVolant();

$nut2->setCoffre("petit");
echo " mais le coffre est ".$nut2->getCoffre();
?>